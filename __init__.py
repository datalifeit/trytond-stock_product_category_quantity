# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import stock, product


def register():
    Pool.register(
        product.Template,
        product.Product,
        stock.Configuration,
        stock.ConfigurationDateCriteria,
        stock.Category,
        stock.ProductsByLocationsStart,
        stock.Location,
        module='stock_product_category_quantity', type_='model')
    Pool.register(
        stock.ProductsByLocations,
        module='stock_product_category_quantity', type_='wizard')
    Pool.register(
        stock.ProductsByLocationsReport,
        module='stock_product_category_quantity', type_='report')
