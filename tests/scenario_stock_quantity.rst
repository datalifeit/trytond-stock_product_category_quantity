========================
Stock Quantity Scenario
========================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> from trytond.tests.tools import activate_modules

Install stock Module::

    >>> config = activate_modules('stock_product_category_quantity')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> config._context = User.get_preferences(True, config.context)

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])

Create products::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> Category = Model.get('product.category')
    >>> cat1 = Category(name='Category 1')
    >>> cat1.stock = True
    >>> cat1.stock_uom_category = unit.category
    >>> child = cat1.childs.new()
    >>> child.name = 'Category 1 child'
    >>> cat1.save()
    >>> bool(cat1.childs[0].stock)
    True
    >>> cat1.childs[0].stock_uom_category == unit.category
    True
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.stock_category = unit.category
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('300')
    >>> template.cost_price = Decimal('80')
    >>> template.cost_price_method = 'average'
    >>> template.categories.append(cat1)
    >>> template.save()
    >>> product, = template.products

    >>> cat2 = Category(name='Category 2')
    >>> cat2.save()
    >>> child = cat2.childs.new()
    >>> child.name = 'Category 2 child'
    >>> bool(cat2.childs[0].stock)
    False
    >>> cat2.childs[0].stock_uom_category == None
    True
    >>> kg, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> template2 = ProductTemplate()
    >>> template2.name = 'Product'
    >>> template2.default_uom = kg
    >>> template2.type = 'goods'
    >>> template2.list_price = Decimal('140')
    >>> template2.cost_price = Decimal('60')
    >>> template2.cost_price_method = 'average'
    >>> template2.categories.append(cat2)
    >>> template2.save()
    >>> product2, = template2.products

Fill storage::

    >>> StockMove = Model.get('stock.move')
    >>> incoming_move = StockMove()
    >>> incoming_move.product = product
    >>> incoming_move.uom = unit
    >>> incoming_move.quantity = 1
    >>> incoming_move.from_location = supplier_loc
    >>> incoming_move.to_location = storage_loc
    >>> incoming_move.planned_date = today
    >>> incoming_move.effective_date = today
    >>> incoming_move.company = company
    >>> incoming_move.unit_price = Decimal('100')
    >>> incoming_move.currency = company.currency
    >>> incoming_moves = [incoming_move]

    >>> incoming_move = StockMove()
    >>> incoming_move.product = product2
    >>> incoming_move.uom = kg
    >>> incoming_move.quantity = 2.5
    >>> incoming_move.from_location = supplier_loc
    >>> incoming_move.to_location = storage_loc
    >>> incoming_move.planned_date = today
    >>> incoming_move.effective_date = today
    >>> incoming_move.company = company
    >>> incoming_move.unit_price = Decimal('70')
    >>> incoming_move.currency = company.currency
    >>> incoming_moves.append(incoming_move)
    >>> StockMove.click(incoming_moves, 'do')

Open wizard::

    >>> products_by_loc = Wizard('stock.products_by_locations', [storage_loc])
    >>> products_by_loc.form.with_cat_childs
    True
    >>> products_by_loc.execute('open')

Open wizard and report::

    >>> Data = Model.get('ir.model.data')
    >>> data, = Data.find([('module', '=', 'stock_product_category_quantity'),
    ...                    ('fs_id', '=', 'wizard_products_by_locations')])
    >>> _ = config._context.setdefault('action_id', data.db_id)
    >>> products_by_loc = Wizard('stock.products_by_locations', [storage_loc])
    >>> products_by_loc.execute('open')

Product category quantity::

    >>> _ = config._context.setdefault('locations', [storage_loc.id])
    >>> _ = config._context.setdefault('stock_date_end', today)
    >>> cat1 = Category(cat1.id)
    >>> cat1.quantity
    1.0
